# Polish translation for fractal.
# Copyright © 2018-2020 the fractal authors.
# This file is distributed under the same license as the fractal package.
# Marek Jędrzejewski <kontakt@marekjedrzejewski.pl>, 2018.
# Piotr Drąg <piotrdrag@gmail.com>, 2018-2020.
# Aviary.pl <community-poland@mozilla.org>, 2018-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: fractal\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/fractal/issues\n"
"POT-Creation-Date: 2020-07-29 06:07+0000\n"
"PO-Revision-Date: 2020-08-02 13:33+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: fractal-gtk/res/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "Ogólne"

#: fractal-gtk/res/gtk/help-overlay.ui:18
msgctxt "shortcut window"
msgid "Close the active room"
msgstr "Zamknięcie aktywnego pokoju"

#: fractal-gtk/res/gtk/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Open main menu"
msgstr "Otwarcie głównego menu"

#: fractal-gtk/res/gtk/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Open / close the room sidebar search"
msgstr "Otwarcie/zamknięcie wyszukiwania pokoju w panelu bocznym"

#: fractal-gtk/res/gtk/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Open the previous room in the list"
msgstr "Otwarcie poprzedniego pokoju z listy"

#: fractal-gtk/res/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Open the next room in the list"
msgstr "Otwarcie następnego pokoju z listy"

#: fractal-gtk/res/gtk/help-overlay.ui:53
msgctxt "shortcut window"
msgid "Open the previous room with unread messages in the list"
msgstr "Otwarcie poprzedniego pokoju z nieprzeczytanymi wiadomościami z listy"

#: fractal-gtk/res/gtk/help-overlay.ui:60
msgctxt "shortcut window"
msgid "Open the next room with unread messages in the list"
msgstr "Otwarcie następnego pokoju z nieprzeczytanymi wiadomościami z listy"

#: fractal-gtk/res/gtk/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Open the first room of the list"
msgstr "Otwarcie pierwszego pokoju z listy"

#: fractal-gtk/res/gtk/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Open the last room of the list"
msgstr "Otwarcie ostatniego pokoju z listy"

#: fractal-gtk/res/gtk/help-overlay.ui:81
msgctxt "shortcut window"
msgid "View older messages"
msgstr "Wyświetlenie starszych wiadomości"

#: fractal-gtk/res/gtk/help-overlay.ui:88
msgctxt "shortcut window"
msgid "View newer messages"
msgstr "Wyświetlenie nowszych wiadomości"

#: fractal-gtk/res/gtk/help-overlay.ui:94
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Skróty klawiszowe"

#: fractal-gtk/res/gtk/help-overlay.ui:101
msgctxt "shortcut window"
msgid "Quit"
msgstr "Zakończenie działania"

#: fractal-gtk/res/gtk/help-overlay.ui:110
msgctxt "shortcut message"
msgid "Composing a new message"
msgstr "Utworzenie nowej wiadomości"

#: fractal-gtk/res/gtk/help-overlay.ui:115
msgctxt "shortcut message"
msgid "Write on a new line"
msgstr "Pisanie w nowym wierszu"

#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:3
#: fractal-gtk/res/org.gnome.Fractal.metainfo.xml.in.in:5
#: fractal-gtk/res/ui/login_flow.ui:294 fractal-gtk/src/appop/mod.rs:213
msgid "Fractal"
msgstr "Fractal"

#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:4
msgid ""
"Fractal is a decentralized, secure messaging client for collaborative group "
"communication."
msgstr ""
"Fractal jest zdecentralizowanym, bezpiecznym komunikatorem do komunikacji "
"wśród współpracujących grup."

#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:5
msgid "Fractal group messaging"
msgstr "Wiadomości grupowe Fractal"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: fractal-gtk/res/org.gnome.Fractal.desktop.in.in:14
msgid "Matrix;matrix.org;chat;irc;communications;talk;riot;"
msgstr ""
"Matrix;matrix.org;czat;chat;irc;komunikacja;komunikator;im;wiadomość;rozmowa;"
"riot;"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:13
msgid "Type of password and token storage"
msgstr "Typ magazynu hasła i tokenu"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:14
msgid "Type of password and token storage, default value is: Secret Service"
msgstr "Typ magazynu hasła i tokenu, domyślą wartością jest: Secret Service"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:21
msgid "If markdown sending is active"
msgstr "Czy wysyłanie Markdown jest aktywne"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:22
msgid "Whether support for sending markdown messages is on"
msgstr "Czy obsługa wysyłania wiadomości Markdown jest włączona"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:29
msgid "X position of the main window on startup"
msgstr "Położenie głównego okna na osi X podczas uruchamiania"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:34
msgid "Y position of the main window on startup"
msgstr "Położenie głównego okna na osi Y podczas uruchamiania"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:39
msgid "Width of the main window on startup"
msgstr "Szerokość głównego okna podczas uruchamiania"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:44
msgid "Height of the main window on startup"
msgstr "Wysokość głównego okna podczas uruchamiania"

#: fractal-gtk/res/org.gnome.Fractal.gschema.xml:49
msgid "Whether the main window is maximized on startup"
msgstr "Czy główne okno jest zmaksymalizowane podczas uruchamiania"

#: fractal-gtk/res/org.gnome.Fractal.metainfo.xml.in.in:8
msgid "Daniel García Moreno"
msgstr "Daniel García Moreno"

#: fractal-gtk/res/org.gnome.Fractal.metainfo.xml.in.in:9
msgid "Matrix group messaging app"
msgstr "Rozmowy grupowe w sieci Matrix"

#: fractal-gtk/res/org.gnome.Fractal.metainfo.xml.in.in:11
msgid ""
"Fractal is a Matrix messaging app for GNOME written in Rust. Its interface "
"is optimized for collaboration in large groups, such as free software "
"projects."
msgstr ""
"Fractal jest komunikatorem sieci Matrix dla środowiska GNOME napisanym "
"w języku Rust. Jego interfejs jest zoptymalizowany do współpracy dużych "
"grup, takich jak projekty wolnego oprogramowania."

#: fractal-gtk/res/ui/account_settings.ui:73
msgid "Other people can find you by searching for any of these identifiers."
msgstr ""
"Inni mogą odnajdywać użytkownika wyszukując dowolne z tych identyfikatorów."

#: fractal-gtk/res/ui/account_settings.ui:101
msgid "Name"
msgstr "Nazwa"

#: fractal-gtk/res/ui/account_settings.ui:124
msgid "Type in your name"
msgstr "Nazwa użytkownika"

#: fractal-gtk/res/ui/account_settings.ui:153
msgid "Email"
msgstr "Adres e-mail"

#: fractal-gtk/res/ui/account_settings.ui:182
msgid "Phone"
msgstr "Telefon"

#: fractal-gtk/res/ui/account_settings.ui:211
msgid "Password"
msgstr "Hasło"

#: fractal-gtk/res/ui/account_settings.ui:278
msgid "Advanced Information"
msgstr "Zaawansowane informacje"

#: fractal-gtk/res/ui/account_settings.ui:308
msgid "Homeserver"
msgstr "Serwer domowy"

#: fractal-gtk/res/ui/account_settings.ui:337
msgid "Matrix ID"
msgstr "Identyfikator Matrix"

#: fractal-gtk/res/ui/account_settings.ui:366
msgid "Device ID"
msgstr "Identyfikator urządzenia"

#: fractal-gtk/res/ui/account_settings.ui:421
#: fractal-gtk/res/ui/account_settings.ui:502
msgid "Deactivate Account"
msgstr "Dezaktywuj konto"

#: fractal-gtk/res/ui/account_settings.ui:447
msgid ""
"Deactivating your account means that you will lose all your messages, "
"contacts, and files."
msgstr ""
"Dezaktywacja konta oznacza utratę wszystkich wiadomości, kontaktów i plików."

#: fractal-gtk/res/ui/account_settings.ui:462
msgid ""
"To confirm that you really want to deactivate this account type in your "
"password:"
msgstr ""
"Aby potwierdzić, że na pewno to konto ma zostać dezaktywowane, należy wpisać "
"hasło:"

#: fractal-gtk/res/ui/account_settings.ui:487
msgid "Also erase all messages"
msgstr "Usunięcie także wszystkich wiadomości"

#: fractal-gtk/res/ui/account_settings.ui:586
#: fractal-gtk/res/ui/main_menu.ui:109
msgid "Account Settings"
msgstr "Ustawienia konta"

#: fractal-gtk/res/ui/account_settings.ui:602
#: fractal-gtk/res/ui/main_window.ui:522 fractal-gtk/res/ui/main_window.ui:662
#: fractal-gtk/res/ui/main_window.ui:762 fractal-gtk/res/ui/media_viewer.ui:236
#: fractal-gtk/res/ui/room_settings.ui:1034
msgid "Back"
msgstr "Wstecz"

#: fractal-gtk/res/ui/account_settings.ui:623
msgid "Check your email"
msgstr "Proszę sprawdzić pocztę"

#: fractal-gtk/res/ui/account_settings.ui:626
#: fractal-gtk/res/ui/direct_chat.ui:129 fractal-gtk/res/ui/invite_user.ui:129
#: fractal-gtk/res/ui/join_room.ui:94 fractal-gtk/res/ui/leave_room.ui:32
#: fractal-gtk/res/ui/new_room.ui:42 fractal-gtk/src/appop/account.rs:95
#: fractal-gtk/src/appop/account.rs:158 fractal-gtk/src/appop/attach.rs:52
msgid "Cancel"
msgstr "Anuluj"

#: fractal-gtk/res/ui/account_settings.ui:634
msgid "Apply"
msgstr "Zastosuj"

#: fractal-gtk/res/ui/add_room_menu.ui:22
msgid "Room Directory"
msgstr "Katalog pokoi"

#: fractal-gtk/res/ui/add_room_menu.ui:36
msgid "Join Room"
msgstr "Dołącz do pokoju"

#: fractal-gtk/res/ui/add_room_menu.ui:63
msgid "New Room"
msgstr "Nowy pokój"

#: fractal-gtk/res/ui/add_room_menu.ui:77
msgid "New Direct Chat"
msgstr "Nowa rozmowa"

#: fractal-gtk/res/ui/audio_player.ui:34 fractal-gtk/res/ui/audio_player.ui:50
msgid "Play"
msgstr "Odtwarza"

#: fractal-gtk/res/ui/direct_chat.ui:126
msgid "New direct chat"
msgstr "Nowa rozmowa"

#: fractal-gtk/res/ui/direct_chat.ui:137
msgid "Start chat"
msgstr "Rozpocznij rozmowę"

#: fractal-gtk/res/ui/filechooser.ui:61
msgid "Select room image file"
msgstr "Wybór pliku obrazu pokoju"

#: fractal-gtk/res/ui/invite.ui:18
msgid "Invitation"
msgstr "Zaproszenie"

#: fractal-gtk/res/ui/invite.ui:33
msgid "Reject"
msgstr "Odrzuć"

#: fractal-gtk/res/ui/invite.ui:46
msgid "Accept"
msgstr "Przyjmij"

#: fractal-gtk/res/ui/invite_user.ui:126 fractal-gtk/res/ui/invite_user.ui:137
#: fractal-gtk/src/appop/invite.rs:153
msgid "Invite"
msgstr "Zaproś"

#: fractal-gtk/res/ui/join_room.ui:52
msgid "ID or Alias"
msgstr "Identyfikator lub alias"

#: fractal-gtk/res/ui/join_room.ui:91
msgid "Join room"
msgstr "Dołącz do pokoju"

#: fractal-gtk/res/ui/join_room.ui:102 fractal-gtk/src/widgets/room.rs:124
msgid "Join"
msgstr "Dołącz"

#: fractal-gtk/res/ui/kicked_room.ui:30 fractal-gtk/res/ui/msg_src_window.ui:32
msgid "Close"
msgstr "Zamknij"

#: fractal-gtk/res/ui/leave_room.ui:18
msgid "Leave?"
msgstr "Opuścić?"

#: fractal-gtk/res/ui/leave_room.ui:19
msgid ""
"Once you leave, you won’t be able to interact with people in the room "
"anymore."
msgstr ""
"Po opuszczeniu nie będzie można już komunikować się z osobami w pokoju."

#: fractal-gtk/res/ui/leave_room.ui:45
msgid "Leave room"
msgstr "Opuść pokój"

#: fractal-gtk/res/ui/login_flow.ui:33
msgid "Welcome to Fractal"
msgstr "Witamy w programie Fractal"

#: fractal-gtk/res/ui/login_flow.ui:47 fractal-gtk/res/ui/login_flow.ui:371
msgid "_Log In"
msgstr "_Zaloguj się"

#: fractal-gtk/res/ui/login_flow.ui:59
msgid "_Create Account"
msgstr "_Utwórz konto"

#: fractal-gtk/res/ui/login_flow.ui:95
msgid "What is your Provider?"
msgstr "Jakiego dostawcy używać?"

#: fractal-gtk/res/ui/login_flow.ui:124
msgid "Matrix provider domain, e.g. myserver.co"
msgstr "Domena dostawcy Matrix, np. example.com"

#: fractal-gtk/res/ui/login_flow.ui:140
msgid "The domain may not be empty."
msgstr "Domena nie może być pusta."

#: fractal-gtk/res/ui/login_flow.ui:166
msgid "_User ID"
msgstr "_Identyfikator użytkownika"

#: fractal-gtk/res/ui/login_flow.ui:182
msgid "_Password"
msgstr "_Hasło"

#: fractal-gtk/res/ui/login_flow.ui:214
msgid "User name, email, or phone number"
msgstr "Nazwa użytkownika, adres e-mail lub numer telefonu"

#: fractal-gtk/res/ui/login_flow.ui:247
msgid "_Forgot Password?"
msgstr "_Nie pamiętasz hasła?"

#: fractal-gtk/res/ui/login_flow.ui:265
msgid "Invalid username or password"
msgstr "Nieprawidłowa nazwa użytkownika lub hasło"

#: fractal-gtk/res/ui/login_flow.ui:306
msgid "Choose Provider"
msgstr "Wybór dostawcy"

#: fractal-gtk/res/ui/login_flow.ui:329
msgid "_Next"
msgstr "_Dalej"

#: fractal-gtk/res/ui/login_flow.ui:348
msgid "Log In"
msgstr "Logowanie"

#: fractal-gtk/res/ui/main_menu.ui:123
msgid "Log Out"
msgstr "Wyloguj się"

#: fractal-gtk/res/ui/main_menu.ui:150
msgid "Keyboard _Shortcuts"
msgstr "_Skróty klawiszowe"

#: fractal-gtk/res/ui/main_menu.ui:164
msgid "_About Fractal"
msgstr "_O programie"

#: fractal-gtk/res/ui/main_window.ui:185
msgid "No room selected"
msgstr "Nie wybrano pokoju"

#: fractal-gtk/res/ui/main_window.ui:200
msgid "Join a room to start chatting"
msgstr "Dołączenie do pokoju umożliwi rozpoczęcie rozmowy"

#: fractal-gtk/res/ui/main_window.ui:210
msgid "No room"
msgstr "Brak pokoju"

#: fractal-gtk/res/ui/main_window.ui:291
msgid "Chat"
msgstr "Rozmowa"

#: fractal-gtk/res/ui/main_window.ui:343
msgid "Directory"
msgstr "Katalog"

#: fractal-gtk/res/ui/main_window.ui:367
msgid "Loading"
msgstr "Wczytywanie"

#: fractal-gtk/res/ui/main_window.ui:412
msgid "User"
msgstr "Użytkownik"

#: fractal-gtk/res/ui/main_window.ui:439
msgid "Add"
msgstr "Dodaj"

#: fractal-gtk/res/ui/main_window.ui:469
msgid "Room search"
msgstr "Wyszukiwanie pokoju"

#: fractal-gtk/res/ui/main_window.ui:596
msgid "Room Menu"
msgstr "Menu pokoju"

#: fractal-gtk/res/ui/main_window.ui:702
#: fractal-gtk/res/ui/server_chooser_menu.ui:65
#: fractal-gtk/src/app/connect/directory.rs:137
msgid "Default Matrix Server"
msgstr "Domyślny serwer Matrix"

#: fractal-gtk/res/ui/markdown_popover.ui:23
msgid "Markdown"
msgstr "Formatowanie Markdown"

#: fractal-gtk/res/ui/markdown_popover.ui:67
msgid "> quote"
msgstr "> cytat"

#: fractal-gtk/res/ui/markdown_popover.ui:78
msgid "**bold**"
msgstr "**pogrubienie**"

#: fractal-gtk/res/ui/markdown_popover.ui:93
msgid "`code`"
msgstr "`kod`"

#: fractal-gtk/res/ui/markdown_popover.ui:105
msgid "*italic*"
msgstr "*pochylenie*"

#: fractal-gtk/res/ui/media_viewer.ui:173
msgid "Loading more media"
msgstr "Wczytywanie więcej multimediów"

#: fractal-gtk/res/ui/media_viewer.ui:264
msgid "Toggle fullscreen"
msgstr "Przełącza pełny ekran"

#: fractal-gtk/res/ui/members.ui:25 fractal-gtk/res/ui/room_settings.ui:940
msgid "Search for room members"
msgstr "Wyszukiwanie członków pokoju"

#: fractal-gtk/res/ui/message_menu.ui:22
msgid "Reply"
msgstr "Odpowiedz"

#: fractal-gtk/res/ui/message_menu.ui:34
msgid "Open With…"
msgstr "Otwórz za pomocą…"

#: fractal-gtk/res/ui/message_menu.ui:47
msgid "Save Image As…"
msgstr "Zapisz obraz jako…"

#: fractal-gtk/res/ui/message_menu.ui:60
msgid "Save Video As…"
msgstr "Zapisz film jako…"

#: fractal-gtk/res/ui/message_menu.ui:73
msgid "Copy Image"
msgstr "Skopiuj obraz"

#: fractal-gtk/res/ui/message_menu.ui:86
msgid "Copy Selection"
msgstr "Skopiuj zaznaczenie"

#: fractal-gtk/res/ui/message_menu.ui:99
msgid "Copy Text"
msgstr "Skopiuj tekst"

#: fractal-gtk/res/ui/message_menu.ui:114
msgid "View Source"
msgstr "Wyświetl źródło"

#: fractal-gtk/res/ui/message_menu.ui:139
msgid "Delete Message"
msgstr "Usuń wiadomość"

#: fractal-gtk/res/ui/msg_src_window.ui:21
msgid "Message Source"
msgstr "Źródło wiadomości"

#: fractal-gtk/res/ui/msg_src_window.ui:24
msgid "Copy To Clipboard"
msgstr "Skopiuj do schowka"

#: fractal-gtk/res/ui/new_room.ui:14
msgid "Private Chat"
msgstr "Rozmowa prywatna"

#: fractal-gtk/res/ui/new_room.ui:18 fractal-gtk/res/ui/new_room.ui:187
msgid "Public"
msgstr "Publiczny"

#: fractal-gtk/res/ui/new_room.ui:39
msgid "Create new room"
msgstr "Utworzenie nowego pokoju"

#: fractal-gtk/res/ui/new_room.ui:50
msgid "Create"
msgstr "Utwórz"

#: fractal-gtk/res/ui/new_room.ui:108
msgid "Room name"
msgstr "Nazwa pokoju"

#: fractal-gtk/res/ui/new_room.ui:152
msgid "Visibility"
msgstr "Widoczność"

#: fractal-gtk/res/ui/new_room.ui:171
msgid "Private"
msgstr "Prywatny"

#: fractal-gtk/res/ui/password_dialog.ui:22
#: fractal-gtk/src/widgets/file_dialog.rs:12
#: fractal-gtk/src/widgets/file_dialog.rs:33
msgid "_Cancel"
msgstr "_Anuluj"

#: fractal-gtk/res/ui/password_dialog.ui:35
msgid "Ch_ange"
msgstr "_Zmień"

#: fractal-gtk/res/ui/password_dialog.ui:133
msgid "The passwords do not match."
msgstr "Hasła się nie zgadzają."

#: fractal-gtk/res/ui/password_dialog.ui:151
msgid "_Verify New Password"
msgstr "_Sprawdzenie poprawności nowego hasła"

#: fractal-gtk/res/ui/password_dialog.ui:168
msgid "_New Password"
msgstr "_Nowe hasło"

#: fractal-gtk/res/ui/password_dialog.ui:212
msgid "Current _Password"
msgstr "_Obecne hasło"

#: fractal-gtk/res/ui/room_menu.ui:22
msgid "Room Details"
msgstr "Informacje o pokoju"

#: fractal-gtk/res/ui/room_menu.ui:36
msgid "Invite to This Room"
msgstr "Zaproś do tego pokoju"

#: fractal-gtk/res/ui/room_menu.ui:63
msgid "Leave Room"
msgstr "Opuść pokój"

#: fractal-gtk/res/ui/room_settings.ui:92
msgid "Unknown"
msgstr "Nieznane"

#: fractal-gtk/res/ui/room_settings.ui:118
msgid "Add name"
msgstr "Dodawanie nazwy"

#: fractal-gtk/res/ui/room_settings.ui:150
msgid "Add topic"
msgstr "Dodawanie tematu"

#: fractal-gtk/res/ui/room_settings.ui:174
msgid "Type in your room topic"
msgstr "Temat pokoju"

#: fractal-gtk/res/ui/room_settings.ui:202
msgid "No room description"
msgstr "Brak opisu pokoju"

#: fractal-gtk/res/ui/room_settings.ui:239
msgid "Notifications"
msgstr "Powiadomienia"

#: fractal-gtk/res/ui/room_settings.ui:280
msgid "Notification sounds"
msgstr "Dźwięki powiadomień"

#: fractal-gtk/res/ui/room_settings.ui:325
msgid "For all messages"
msgstr "Dla wszystkich wiadomości"

#: fractal-gtk/res/ui/room_settings.ui:366
msgid "Only for mentions"
msgstr "Tylko dla wzmianek"

#: fractal-gtk/res/ui/room_settings.ui:397
msgid "Shared Media"
msgstr "Udostępniane multimedia"

#: fractal-gtk/res/ui/room_settings.ui:444
msgid "photos"
msgstr "zdjęcia"

#: fractal-gtk/res/ui/room_settings.ui:484
msgid "videos"
msgstr "filmy"

#: fractal-gtk/res/ui/room_settings.ui:524
msgid "documents"
msgstr "dokumenty"

#: fractal-gtk/res/ui/room_settings.ui:555
msgid "New members can see"
msgstr "Nowi członkowie mogą widzieć"

#: fractal-gtk/res/ui/room_settings.ui:600
msgid "All room history"
msgstr "Cała historia pokoju"

#: fractal-gtk/res/ui/room_settings.ui:641
msgid "History after they were invited"
msgstr "Historia po zaproszeniu"

#: fractal-gtk/res/ui/room_settings.ui:672
msgid "Room Visibility"
msgstr "Widoczność pokoju"

#: fractal-gtk/res/ui/room_settings.ui:719
msgid "Allow guests"
msgstr "Zezwolenie na gości"

#: fractal-gtk/res/ui/room_settings.ui:759
msgid "Allow joining without invite"
msgstr "Zezwolenie na dołączanie bez zaproszenia"

#: fractal-gtk/res/ui/room_settings.ui:799
msgid "Publish in room directory"
msgstr "Publikowanie w katalogu pokoi"

#: fractal-gtk/res/ui/room_settings.ui:830
msgid "Join addresses"
msgstr "Adresy dołączania"

#: fractal-gtk/res/ui/room_settings.ui:889
msgid "members"
msgstr "członkowie"

#: fractal-gtk/res/ui/room_settings.ui:905
msgid "Invite New Member"
msgstr "Zaproś nowego członka"

#: fractal-gtk/res/ui/room_settings.ui:1018
msgid "Details"
msgstr "Informacje"

#: fractal-gtk/res/ui/scroll_widget.ui:63
msgid "Scroll to bottom"
msgstr "Przewiń na dół"

#: fractal-gtk/res/ui/server_chooser_menu.ui:34
msgid "Show rooms from:"
msgstr "Wyświetlanie pokoi z:"

#: fractal-gtk/res/ui/server_chooser_menu.ui:90
msgid "Your homeserver"
msgstr "Ten serwer domowy"

#: fractal-gtk/res/ui/server_chooser_menu.ui:134
msgid "Other Protocol"
msgstr "Inny protokół"

#: fractal-gtk/res/ui/server_chooser_menu.ui:207
msgid "Other Homeserver"
msgstr "Inny serwer domowy"

#: fractal-gtk/res/ui/server_chooser_menu.ui:235
msgid "Homeserver URL"
msgstr "Adres serwera domowego"

#: fractal-gtk/src/actions/account_settings.rs:34
#: fractal-gtk/src/actions/room_settings.rs:43
msgid "Images"
msgstr "Obrazy"

#: fractal-gtk/src/actions/account_settings.rs:35
#: fractal-gtk/src/actions/room_settings.rs:45
msgid "Select a new avatar"
msgstr "Wybór nowego awatara"

#: fractal-gtk/src/actions/global.rs:298
msgid "Select a file"
msgstr "Wybór pliku"

#: fractal-gtk/src/actions/message.rs:74
msgid "This message has no source."
msgstr "Ta wiadomość nie ma źródła."

#: fractal-gtk/src/actions/message.rs:159
#: fractal-gtk/src/actions/message.rs:197
msgid "Could not download the file"
msgstr "Nie można pobrać pliku"

#: fractal-gtk/src/actions/message.rs:168
msgid "Couldn’t save file"
msgstr "Nie można zapisać pliku"

#: fractal-gtk/src/actions/room_settings.rs:61
msgid "Couldn’t open file"
msgstr "Nie można otworzyć pliku"

#: fractal-gtk/src/appop/about.rs:20
msgid "A Matrix.org client for GNOME"
msgstr "Klient sieci Matrix.org dla środowiska GNOME"

#: fractal-gtk/src/appop/about.rs:22
msgid "© 2017–2018 Daniel García Moreno, et al."
msgstr "© 2017-2018 Daniel García Moreno i inni"

#: fractal-gtk/src/appop/about.rs:29
msgid "Learn more about Fractal"
msgstr "Więcej informacji o programie Fractal"

#: fractal-gtk/src/appop/about.rs:30
msgid "translator-credits"
msgstr ""
"Marek Jędrzejewski <kontakt@marekjedrzejewski.pl>, 2018\n"
"Piotr Drąg <piotrdrag@gmail.com>, 2018-2020\n"
"Aviary.pl <community-poland@mozilla.org>, 2018-2020"

#: fractal-gtk/src/appop/about.rs:45
msgid "Name by"
msgstr "Nazwa"

#: fractal-gtk/src/appop/account.rs:68
msgid "The validation code is not correct."
msgstr "Kod weryfikacyjny jest niepoprawny."

#: fractal-gtk/src/appop/account.rs:81
msgid "Enter the code received via SMS"
msgstr "Proszę wpisać kod otrzymany przez SMS"

#: fractal-gtk/src/appop/account.rs:96 fractal-gtk/src/appop/account.rs:159
msgid "Continue"
msgstr "Kontynuuj"

#: fractal-gtk/src/appop/account.rs:149
msgid ""
"In order to add this email address, go to your inbox and follow the link you "
"received. Once you’ve done that, click Continue."
msgstr ""
"Aby dodać ten adres e-mail, należy przejść do skrzynki odbiorczej i kliknąć "
"otrzymany odnośnik. Po zrobieniu tego należy kliknąć przycisk Kontynuuj."

#: fractal-gtk/src/appop/account.rs:218
msgid "OK"
msgstr "OK"

#: fractal-gtk/src/appop/account.rs:780
msgid "Are you sure you want to delete your account?"
msgstr "Na pewno usunąć konto?"

#: fractal-gtk/src/appop/attach.rs:37
msgid "Image from Clipboard"
msgstr "Obraz ze schowka"

#: fractal-gtk/src/appop/attach.rs:53
msgid "Send"
msgstr "Wyślij"

#. Filename for the attached image
#: fractal-gtk/src/appop/attach.rs:82
msgid "image"
msgstr "obraz"

#: fractal-gtk/src/appop/invite.rs:151
msgid "Invite to {name}"
msgstr "Zaproś do „{name}”"

#: fractal-gtk/src/appop/invite.rs:265
msgid "Join {room_name}?"
msgstr "Dołączyć do pokoju „{room_name}”?"

#: fractal-gtk/src/appop/invite.rs:270
msgid ""
"You’ve been invited to join <b>{room_name}</b> room by <b>{sender_name}</b>"
msgstr ""
"Otrzymano zaproszenie do pokoju <b>{room_name}</b> od <b>{sender_name}</b>"

#: fractal-gtk/src/appop/invite.rs:275
msgid "You’ve been invited to join <b>{room_name}</b>"
msgstr "Otrzymano zaproszenie do pokoju <b>{room_name}</b>"

#. Translators: The placeholder is for the number of unread messages in the
#. application
#: fractal-gtk/src/appop/mod.rs:217
msgid "Fractal [{}]"
msgstr "Fractal ({})"

#: fractal-gtk/src/appop/notify.rs:51
msgid "An audio file has been added to the conversation."
msgstr "Plik dźwiękowy został dodany do rozmowy."

#: fractal-gtk/src/appop/notify.rs:52
msgid "An image has been added to the conversation."
msgstr "Obraz został dodany do rozmowy."

#: fractal-gtk/src/appop/notify.rs:53
msgid "A video has been added to the conversation."
msgstr "Film został dodany do rozmowy."

#: fractal-gtk/src/appop/notify.rs:54
msgid "A file has been added to the conversation."
msgstr "Plik został dodany do rozmowy."

#: fractal-gtk/src/appop/notify.rs:59
msgid " (direct message)"
msgstr " (bezpośrednia wiadomość)"

#: fractal-gtk/src/appop/room.rs:357
msgid "Leave {room_name}?"
msgstr "Opuścić pokój „{room_name}”?"

#: fractal-gtk/src/appop/room.rs:565
msgid "The room ID is malformed"
msgstr "Identyfikator pokoju jest błędnie sformatowany"

#: fractal-gtk/src/appop/room.rs:635
msgid "EMPTY ROOM"
msgstr "PUSTY POKÓJ"

#: fractal-gtk/src/appop/room.rs:637
msgid "{m1} and {m2}"
msgstr "{m1} i {m2}"

#: fractal-gtk/src/appop/room.rs:638
msgid "{m1} and Others"
msgstr "{m1} i inni"

#: fractal-gtk/src/appop/room.rs:737
msgid "Several users are typing…"
msgstr "Kilku użytkowników pisze…"

#: fractal-gtk/src/appop/room.rs:740
msgid "<b>{}</b> is typing…"
msgid_plural "<b>{}</b> and <b>{}</b> are typing…"
msgstr[0] "<b>{}</b> pisze…"
msgstr[1] "<b>{}</b> i <b>{}</b> piszą…"
msgstr[2] "<b>{}</b> i <b>{}</b> piszą…"

#: fractal-gtk/src/appop/sync.rs:16
msgid "Syncing, this could take a while"
msgstr "Synchronizowanie. Może to chwilę zająć."

#: fractal-gtk/src/backend/directory.rs:39
#: fractal-gtk/src/backend/directory.rs:80
msgid "Error searching for rooms"
msgstr "Błąd podczas wyszukiwania pokoi"

#: fractal-gtk/src/backend/register.rs:37
msgid "Can’t login, try again"
msgstr "Nie można się zalogować. Proszę spróbować ponownie."

#: fractal-gtk/src/backend/room.rs:349
msgid "Error deleting message"
msgstr "Błąd podczas usuwania wiadomości"

#: fractal-gtk/src/backend/room.rs:394
msgid "Can’t join the room, try again."
msgstr "Nie można dołączyć do pokoju. Proszę spróbować ponownie."

#: fractal-gtk/src/backend/room.rs:653 fractal-gtk/src/backend/room.rs:710
msgid "Can’t create the room, try again"
msgstr "Nie można utworzyć pokoju. Proszę spróbować ponownie."

#: fractal-gtk/src/backend/user.rs:161
msgid "Sorry, account settings can’t be loaded."
msgstr "Nie można wczytać ustawień konta."

#: fractal-gtk/src/backend/user.rs:200
msgid "Email is already in use"
msgstr "Adres e-mail jest już używany"

#: fractal-gtk/src/backend/user.rs:204
msgid "Please enter a valid email address."
msgstr "Proszę podać prawidłowy adres e-mail."

#: fractal-gtk/src/backend/user.rs:208
msgid "Couldn’t add the email address."
msgstr "Nie można dodać adresu e-mail."

#: fractal-gtk/src/backend/user.rs:217 fractal-gtk/src/backend/user.rs:295
msgid "The identity server is invalid."
msgstr "Serwer tożsamości jest nieprawidłowy."

#: fractal-gtk/src/backend/user.rs:276
msgid "Phone number is already in use"
msgstr "Numer telefonu jest już używany"

#: fractal-gtk/src/backend/user.rs:281
msgid ""
"Please enter your phone number in the format: \n"
" + your country code and your phone number."
msgstr ""
"Proszę podać numer telefonu w formacie: \n"
" + kod kraju i numer telefonu."

#: fractal-gtk/src/backend/user.rs:286
msgid "Couldn’t add the phone number."
msgstr "Nie można dodać numeru telefonu."

#: fractal-gtk/src/backend/user.rs:442
msgid "Couldn’t change the password"
msgstr "Nie można zmienić hasła"

#: fractal-gtk/src/backend/user.rs:486
msgid "Couldn’t delete the account"
msgstr "Nie można usunąć konta"

#: fractal-gtk/src/uibuilder.rs:52
msgid "You don’t have permission to post to this room"
msgstr "Brak uprawnienia do pisania wiadomości w tym pokoju"

#: fractal-gtk/src/widgets/file_dialog.rs:8
msgid "Save media as"
msgstr "Zapis multimediów jako"

#: fractal-gtk/src/widgets/file_dialog.rs:11
msgid "_Save"
msgstr "_Zapisz"

#: fractal-gtk/src/widgets/file_dialog.rs:32
msgid "_Select"
msgstr "_Wybierz"

#: fractal-gtk/src/widgets/inline_player.rs:521
msgid "Could not retrieve file URI"
msgstr "Nie można odebrać adresu URI pliku"

#: fractal-gtk/src/widgets/kicked_dialog.rs:46
msgid "You have been kicked from {}"
msgstr "Użytkownik został wyrzucony z pokoju {}"

#: fractal-gtk/src/widgets/kicked_dialog.rs:50
msgid ""
"Kicked by: {}\n"
" “{}”"
msgstr ""
"Przez użytkownika: {}\n"
" „{}”"

#: fractal-gtk/src/widgets/login.rs:98
msgid "Malformed server URL"
msgstr "Błędnie sformatowany adres serwera"

#: fractal-gtk/src/widgets/media_viewer.rs:1050
msgid "Error while loading previous media"
msgstr "Błąd podczas wczytywania poprzednich multimediów"

#: fractal-gtk/src/widgets/members_list.rs:51
msgid "No matching members found"
msgstr "Nie odnaleziono pasujących członków"

#: fractal-gtk/src/widgets/members_list.rs:167
msgid "Admin"
msgstr "Administrator"

#: fractal-gtk/src/widgets/members_list.rs:168
msgid "Moderator"
msgstr "Moderator"

#: fractal-gtk/src/widgets/members_list.rs:169
msgid "Privileged"
msgstr "Z uprawnieniami"

#: fractal-gtk/src/widgets/message.rs:90
msgid "Uploading video."
msgstr "Wysyłanie filmu."

#: fractal-gtk/src/widgets/message.rs:95
msgid "Uploading audio."
msgstr "Wysyłanie dźwięku."

#: fractal-gtk/src/widgets/message.rs:100
msgid "Uploading image."
msgstr "Wysyłanie obrazu."

#: fractal-gtk/src/widgets/message.rs:104
msgid "Uploading file."
msgstr "Wysyłanie pliku."

#: fractal-gtk/src/widgets/message.rs:256
#, c-format
msgid "Last edited %c"
msgstr "Ostatnia modyfikacja: %c"

#: fractal-gtk/src/widgets/message.rs:480
#: fractal-gtk/src/widgets/message.rs:595
msgid "Save"
msgstr "Zapisuje"

#: fractal-gtk/src/widgets/message.rs:609
msgid "Open"
msgstr "Otwiera"

#. Use 12h time format (AM/PM)
#: fractal-gtk/src/widgets/message.rs:629
msgid "%l∶%M %p"
msgstr "%-l∶%M %p"

#. Use 24 time format
#: fractal-gtk/src/widgets/message.rs:632
msgid "%R"
msgstr "%H∶%M"

#: fractal-gtk/src/widgets/room_history.rs:96
msgid "New Messages"
msgstr "Nowe wiadomości"

#. Translators: This is a date format in the day divider without the year
#: fractal-gtk/src/widgets/room_history.rs:795
msgid "%B %e"
msgstr "%-d %B"

#. Translators: This is a date format in the day divider with the year
#: fractal-gtk/src/widgets/room_history.rs:798
msgid "%B %e, %Y"
msgstr "%-d %B %Y"

#: fractal-gtk/src/widgets/room_settings.rs:211
msgid "Room · {} member"
msgid_plural "Room · {} members"
msgstr[0] "Pokój · {} członek"
msgstr[1] "Pokój · {} członków"
msgstr[2] "Pokój · {} członków"

#: fractal-gtk/src/widgets/room_settings.rs:659
msgid "{} member"
msgid_plural "{} members"
msgstr[0] "{} członek"
msgstr[1] "{} członków"
msgstr[2] "{} członków"

#: fractal-gtk/src/widgets/roomlist.rs:480
msgid "Invites"
msgstr "Zaproszenia"

#: fractal-gtk/src/widgets/roomlist.rs:481
msgid "You don’t have any invitations"
msgstr "Nie ma żadnych zaproszeń"

#: fractal-gtk/src/widgets/roomlist.rs:485
msgid "Favorites"
msgstr "Ulubione"

#: fractal-gtk/src/widgets/roomlist.rs:486
msgid "Drag and drop rooms here to add them to your favorites"
msgstr "Przeciągnięcie pokoi tutaj doda je do ulubionych"

#: fractal-gtk/src/widgets/roomlist.rs:490
msgid "Rooms"
msgstr "Pokoje"

#: fractal-gtk/src/widgets/roomlist.rs:491
msgid "You don’t have any rooms yet"
msgstr "Nie ma jeszcze żadnych pokoi"
